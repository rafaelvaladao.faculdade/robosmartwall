package rt01;

import robocode.HitRobotEvent;
import robocode.Robot;
import robocode.ScannedRobotEvent;

import java.awt.*;

public class SmartWall extends Robot {

	boolean peek; // Don't turn if there's a robot there
	double moveAmount; // How much to move

	/**
	 * run: Move around the walls
	 */
	public void run() {
		// Set colors
		setBodyColor(new Color(0, 200, 0));
		setGunColor(new Color(0, 200, 0));
		setRadarColor(new Color(0, 200, 0));
		setBulletColor(new Color(0, 200, 0));
		setScanColor(new Color(0, 200, 0));

		// Initialize moveAmount to the maximum possible for this battlefield.
		moveAmount = Math.max(getBattleFieldWidth(), getBattleFieldHeight());
		// Initialize peek to false
		peek = false;

		// turnLeft to face a wall.
		// getHeading() % 90 means the remainder of
		// getHeading() divided by 90.
		turnLeft(getHeading() % 90);
		ahead(moveAmount);
		// Turn the gun to turn right 90 degrees.
		peek = true;
		turnGunRight(90);
		turnRight(90);

		while (true) {
			// Look before we turn when ahead() completes.
			peek = true;
			// Move up the wall
			ahead(moveAmount);
			// Don't look now
			peek = false;
			// Turn to the next wall
			turnRight(90);
		}
	}

	/**
	 * onHitRobot:  Move away a bit.
	 */
	public void onHitRobot(HitRobotEvent e) {
		// If he's in front of us, set back up a bit.
		if (e.getBearing() > -90 && e.getBearing() < 90) {
			back(100);
		} // else he's in back of us, so set ahead a bit.
		else {
			ahead(100);
		}
	}

	/**
	 * onScannedRobot:  Fire!
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		smartFire(e.getDistance());
		// Note that scan is called automatically when the robot is moving.
		// By calling it manually here, we make sure we generate another scan event if there's a robot on the next
		// wall, so that we do not start moving up it until it's gone.
		if (peek) {
			scan();
		}
		// Adicionei o mesmo movimento do onHitRobot, com o objetivo de evitar mais colisões.
		if (e.getDistance() <= 50){
			if (e.getBearing() > -90 && e.getBearing() < 90) {
				back(100);
			}
			else {
				ahead(100);
			}
		}
	}

// Adicionei aqui uma versão modificada do smartFire usado no robo "Corners"
// Aqui é considerado a quantidade de oponentes em campo, ele atira mais "descuidado" quando tem mais oponentes pois tem muita chance de acertar-los
// Atira mais cuidadosamente de acordo diminui a quantidade de inimigos, ja que a chance de acerto vai ser menor.
public void smartFire(double robotDistance) {
		if (getOthers() < 3){
			if (robotDistance > 200 || getEnergy() < 15) {
				fire(1);
			} else if (robotDistance > 50) {
				fire(2);
			} else {
				fire(3);
			}
		} else if (getOthers() < 5){
			fire(2);
		} else {
			fire(3);
		}
	}
}
